package com.maxim.recyclerview;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.maxim.recyclerview.model.Contact;

import static com.maxim.recyclerview.MainActivity.KEY;

public class Main2Activity extends AppCompatActivity {

    TextView name;
    TextView email;
    TextView address;
    TextView telephone;
    ImageView icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        name = (TextView) findViewById(R.id.textView_name_icon);
        email = (TextView) findViewById(R.id.textView_email_icon);
        address = (TextView) findViewById(R.id.textView_address_icon);
        telephone = (TextView) findViewById(R.id.textView_telephone_icon);
        icon = (ImageView) findViewById(R.id.imageView_icon);

        if (getIntent() != null) {
            Contact contact = (Contact) getIntent().getSerializableExtra(KEY);
            name.setText(contact.getName());
            email.setText(contact.getEmail());
            address.setText(contact.getAddress());
            telephone.setText(contact.getTelephone());
            icon.setImageResource(contact.getIcon());
        }
    }

}
