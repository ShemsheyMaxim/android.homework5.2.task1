package com.maxim.recyclerview;

import android.content.Context;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.maxim.recyclerview.model.Contact;

import java.util.List;

/**
 * Created by Максим on 26.05.2017.
 */

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ViewHolder> {

    interface ContactClickListener {
        void onClick(Contact contact);
    }

    interface ItemClickListener {
        void onClick(int position);
    }

    private Context context;
    private List<Contact> contacts;
    private ContactClickListener contactClickListener;
    private ItemClickListener itemClickListener = new ItemClickListener() {
        @Override
        public void onClick(int position) {
            contactClickListener.onClick(contacts.get(position));
        }
    };

    public ContactAdapter(Context context, List<Contact> contacts, ContactClickListener contactClickListener) {
        this.context = context;
        this.contacts = contacts;
        this.contactClickListener = contactClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(context).inflate(R.layout.item_view, parent, false);
        return new ViewHolder(item);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Contact contact = contacts.get(position);
        holder.name.setText(contact.getName());
        holder.email.setText(contact.getEmail());
        holder.icon.setImageResource(contact.getIcon());
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView name;
        private TextView email;
        private ImageView icon;

        public ViewHolder(View item) {
            super(item);
            name = (TextView) item.findViewById(R.id.textView_name);
            email = (TextView) item.findViewById(R.id.textView_email);
            icon = (ImageView) item.findViewById(R.id.imageView);
            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.onClick(getAdapterPosition());
                }
            });
        }
    }
}
