package com.maxim.recyclerview;

import android.content.Intent;
import android.content.res.Resources;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.maxim.recyclerview.model.Contact;

import java.util.ArrayList;
import java.util.List;

import static android.R.id.list;
import static android.provider.Contacts.SettingsColumns.KEY;

public class MainActivity extends AppCompatActivity implements ContactAdapter.ContactClickListener {

    public static final String KEY = "contact";

    RecyclerView recyclerView;
    List<Contact> contacts = new ArrayList<Contact>();
    ContactAdapter contactAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getData();
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayout.HORIZONTAL);
        recyclerView.setLayoutManager(llm);

        contactAdapter = new ContactAdapter(this, contacts, this);
        recyclerView.setAdapter(contactAdapter);

        FloatingActionButton myFAB = (FloatingActionButton) findViewById(R.id.fab);
        myFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Main3Activity.class);
                startActivityForResult(intent, 1);
            }
        });
    }

    @Override
    public void onClick(Contact contact) {
        Intent intent = new Intent(this, Main2Activity.class);
        intent.putExtra(KEY, contact);
        startActivity(intent);
    }

    private void getData() {

        int[] arrayIcons = {R.drawable.user_male_96,
                R.drawable.cristiano_ronaldo_96,
                R.drawable.donald_trump_96,
                R.drawable.einstein_100,
                R.drawable.kim_kardashian_96,
                R.drawable.beyonce_96,
                R.drawable.billie_holiday_96,
                R.drawable.groucho_marx_96,
                R.drawable.messi_96,
                R.drawable.peter_the_great_96,
                R.drawable.super_mario_100,
                R.drawable.ronaldo_96};

        Resources resource = getResources();
        String[] names = resource.getStringArray(R.array.names);
        String[] email = resource.getStringArray(R.array.email);
        String[] address = resource.getStringArray(R.array.address);
        String[] telephone = resource.getStringArray(R.array.telephone);

        for (int i = 0; i < names.length; i++) {
            contacts.add(new Contact(names[i], email[i], address[i], telephone[i], arrayIcons[i]));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (intent == null) {
            return;
        }
        String name = intent.getStringExtra("nameContact");
        String email = intent.getStringExtra("emailContact");
        String address = intent.getStringExtra("addressContact");
        String telephone = intent.getStringExtra("telephoneContact");
//        int image = Integer.parseInt(intent.getStringExtra("imageViewNewContact"));
//        int image = intent.getIntExtra("imageViewNewContact")
        contacts.add(new Contact(name, email, address, telephone, R.drawable.image_file_96));
        contactAdapter.notifyDataSetChanged();
    }
}